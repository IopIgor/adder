#ifndef Adder_HPP
#define Adder_HPP

#include <systemc.h>
#include <iostream>

using namespace std;
using namespace sc_core;

SC_MODULE(Adder) 
{
  static const unsigned bit_data = 32;

  sc_in<sc_bv<bit_data> > A;
  sc_in<sc_bv<bit_data> > B;
  sc_out<sc_bv<bit_data> > Y;
  
  SC_CTOR(Adder) 
  {
    SC_THREAD(behav);
        sensitive << A << B;
  } 
  
 private:
  void behav();
};

#endif
