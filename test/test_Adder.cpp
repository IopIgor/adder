#include <systemc.h>
#include <iostream>
#include "Adder.hpp"

using namespace std;
using namespace sc_core;

const sc_time wait_time(1, SC_NS);
const unsigned ele_vectors = 5;
const unsigned bit_data = 32;
const sc_bv<bit_data> A_vector[ele_vectors] = {10, 0, 0, 8, 7};
const sc_bv<bit_data> B_vector[ele_vectors] = {10, 10, 0, 6, 2};
const sc_bv<bit_data> Y_vector_theor[ele_vectors] = {20, 10, 0, 14, 9};

SC_MODULE(TestBench) 
{
 public:
  sc_signal<sc_bv<bit_data> > A;
  sc_signal<sc_bv<bit_data> > B;
  sc_signal<sc_bv<bit_data> > Y;

  Adder add_test;

  SC_CTOR(TestBench) : add_test("add_test")
  {
    SC_THREAD(stimulus_thread);
    SC_THREAD(watcher_thread);
      sensitive << A << B;

    add_test.A(this->A);
    add_test.B(this->B);
	  add_test.Y(this->Y);
  }

  bool check() 
  {
    bool error = 0;
    for(unsigned i = 0; i < ele_vectors; i++)
    {
      if(Y_vector_theor[i] != Y_vector_res[i])
      {
        cout << "test failed!!\nTheoretical value: " << (Y_vector_theor[i]).to_uint()
             << "\ntest result: " <<  (Y_vector_res[i]).to_uint() << endl << endl;
        error = 1;
      }
    }
    return error;
  }
  
 private:
  sc_bv<bit_data> Y_vector_res[ele_vectors];

  void stimulus_thread() 
  {
    for(unsigned i = 0; i < ele_vectors; i++)
    { 
      A.write(A_vector[i]);
      B.write(B_vector[i]);
      wait(wait_time);
      Y_vector_res[i] = Y.read();
    }
  }

  void watcher_thread() 
  {
    while(true)
    {
       wait();
       wait(1, SC_PS);
       cout << "At time " << sc_time_stamp() << ": The new inputs are " << A.read().to_uint()
            << " and " << B.read().to_uint() << ", and the result is " << Y.read().to_uint() 
            <<  endl;
    }
  }
};

int sc_main(int argc, char** argv) 
{
  TestBench test("test");

  sc_start();

  return test.check();
}

