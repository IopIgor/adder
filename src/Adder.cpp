#include <systemc.h>
#include "Adder.hpp"

using namespace std;
using namespace sc_core;

void Adder::behav()
{
    while(true)
    {
      wait();
      Y->write(A->read().to_uint() + B->read().to_uint());
      
    }
}
